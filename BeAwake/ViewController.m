//
//  ViewController.m
//  BeAwake
//
//  Created by click labs136 on 9/24/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "ViewController.h"
#import "DisclaimerView.h"

@interface ViewController ()

@end

@implementation ViewController
{
    DisclaimerView* disclaimerViewObject;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self firstView];
}

-(void)firstView{
    disclaimerViewObject= [[DisclaimerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [disclaimerViewObject disclaimerbar];
    [self.view addSubview:disclaimerViewObject];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
