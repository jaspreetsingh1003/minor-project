//
//  selectOptionsView.m
//  BeAwake
//
//  Created by click labs136 on 9/26/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "selectOptionsView.h"
#import "FinalView.h"

@implementation selectOptionsView
{
    UIView *screenViewobject;
    NSMutableArray*tripTypeArray;
    NSMutableArray*tiredlevelArray;
    NSMutableArray*alertTypeArray;
    UIButton* tripTypeButton;
    UIButton *tiredlevelButton;
    UIButton* alertTypeButton;
   UIPickerView *tripTypePickerView;
    UIPickerView *tiredlevelPickerView;
    UIPickerView *alertTypePickerView;
}

-(void)selectOptionsViewMethod{
    [self setBackgroundColor:[UIColor yellowColor]];
    [self screenView];
}

-(void)screenView{
    screenViewobject= [[UIView alloc]initWithFrame:CGRectMake(0, 150, self.frame.size.width, 250)];
    [screenViewobject setBackgroundColor:[UIColor grayColor]];
    [self addSubview:screenViewobject];
    [self allButtons];
    [self allLabels];
    
    tripTypeArray= [[NSMutableArray alloc]initWithObjects:@"Friends",@"Family",@"Alone", nil];
    
    tiredlevelArray= [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
    
    alertTypeArray= [[NSMutableArray alloc]initWithObjects:@"Normal",@"Random", nil];
    
    
 }


-(void)allLabels{

    UILabel* tripTypeLabel= [[UILabel alloc]initWithFrame:CGRectMake(30, 20, 100, 30)];
    [tripTypeLabel setText:@"  Trip type"];
    [tripTypeLabel setBackgroundColor:[UIColor blackColor]];
    [tripTypeLabel setTextColor:[UIColor whiteColor]];
    [screenViewobject addSubview:tripTypeLabel];
    
    
    UILabel* tiredLevelLabel= [[UILabel alloc]initWithFrame:CGRectMake(30, 80, 100, 30)];
    [tiredLevelLabel setText:@"  Tired level"];
    [tiredLevelLabel setBackgroundColor:[UIColor blackColor]];
    [tiredLevelLabel setTextColor:[UIColor whiteColor]];
    [screenViewobject addSubview:tiredLevelLabel];


    UILabel* alertTypeLabel= [[UILabel alloc]initWithFrame:CGRectMake(30, 140, 100, 30)];
    [alertTypeLabel setText:@"  Alert type"];
    [alertTypeLabel setBackgroundColor:[UIColor blackColor]];
    [alertTypeLabel setTextColor:[UIColor whiteColor]];
    [screenViewobject addSubview:alertTypeLabel];

}


-(void)allButtons{
    
    tripTypeButton= [[UIButton alloc]initWithFrame:CGRectMake(200, 20, 100, 30)];
    [tripTypeButton setBackgroundColor:[UIColor blueColor]];
    [tripTypeButton addTarget:self action:@selector(tripTypeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [screenViewobject addSubview:tripTypeButton];
    
    
    tiredlevelButton= [[UIButton alloc]initWithFrame:CGRectMake(200, 80, 100, 30)];
    [tiredlevelButton setBackgroundColor:[UIColor blueColor]];
    [tiredlevelButton addTarget:self action:@selector(tiredlevelButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [screenViewobject addSubview:tiredlevelButton];
    
    alertTypeButton= [[UIButton alloc]initWithFrame:CGRectMake(200, 140, 100, 30)];
    [alertTypeButton setBackgroundColor:[UIColor blueColor]];
    [alertTypeButton addTarget:self action:@selector(alertTypeButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [screenViewobject addSubview:alertTypeButton];
    
    
    
//    UIButton *_button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_button setFrame:CGRectMake(200.f, 0.f, 128.f, 128.f)]; // SET the values for your wishes
//    [_button setCenter:CGPointMake(250.f, 150.f)]; // SET the values for your wishes
//    [_button setClipsToBounds:false];
//    [_button setBackgroundImage:[UIImage imageNamed:@"scalar.png"] forState:UIControlStateNormal]; // SET the image name for your wishes
//    [_button setTitle:@"yo man" forState:UIControlStateNormal];
//    [_button.titleLabel setFont:[UIFont systemFontOfSize:24.f]];
//    [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal]; // SET the colour for your wishes
//    [_button setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted]; // SET the colour for your wishes
//    [_button setTitleEdgeInsets:UIEdgeInsetsMake(0.f, -50.f, -80.f, 0.f)]; // SET the values for your wishes
////    [_button addTarget:self action:@selector(buttonTouchedUpInside:) forControlEvents:UIControlEventTouchUpInside];
//    [screenViewobject addSubview:_button];
    
    UIButton *activateButton= [[UIButton alloc]initWithFrame:CGRectMake(90, 400, 150, 30)];
    UIImage *activateButtonImage = [UIImage imageNamed:@"3_Home_Screen.png"];
    [activateButton setBackgroundImage:activateButtonImage forState:UIControlStateNormal];
    [self addSubview:activateButton];
    
    
    
}

-(void)tripTypeButtonAction{
    
   // if (! tripTypePickerView )
    tripTypePickerView= [[UIPickerView alloc]initWithFrame:CGRectMake(0, 400, self.frame.size.width, 200)];
    
    tripTypePickerView.dataSource= self;
    tripTypePickerView.delegate= self;
    tripTypePickerView.tag = 101;
 [self addSubview:tripTypePickerView];
    
}

-(void)tiredlevelButtonAction{
    
    //if (! tiredlevelPickerView)
    tiredlevelPickerView= [[UIPickerView alloc]initWithFrame:CGRectMake(0, 400, self.frame.size.width, 200)];
    tiredlevelPickerView.dataSource= self;
    tiredlevelPickerView.delegate= self;
    tiredlevelPickerView.tag = 102;
    [self addSubview:tiredlevelPickerView];
    
}

-(void)alertTypeButtonAction{
    
   // if (!alertTypePickerView)
    alertTypePickerView= [[UIPickerView alloc]initWithFrame:CGRectMake(0, 400, self.frame.size.width, 200)];
    alertTypePickerView.dataSource= self;
    alertTypePickerView.delegate= self;
    alertTypePickerView.tag= 103;
    [self addSubview:alertTypePickerView];

}

-(void)buttonTouchedUpInside{
}


#pragma mark - All Picker view methods


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}




-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
    if ( pickerView.tag == 101 )
    {
        return  [tripTypeArray objectAtIndex:row];
    
    }
    if (pickerView.tag == 102)
    {
        return  [tiredlevelArray objectAtIndex:row];
    }
    if (pickerView.tag == 103)
    {
        return [alertTypeArray objectAtIndex:row];
    }
   else
    
       return nil;

    
}




-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (pickerView.tag == 101)
    {
        return tripTypeArray.count;
    }
    if (pickerView.tag == 102) {
        return  tiredlevelArray.count;
    }
    if (pickerView.tag == 103){
        return alertTypeArray.count;
    }
    else
        return 0;
}





-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView.tag == 101)
    {
    [tripTypeButton setTitle:[tripTypeArray objectAtIndex:row] forState:UIControlStateNormal];
        pickerView.hidden=YES;
        
    }
    if (pickerView.tag == 102)
    {
        [tiredlevelButton setTitle:[tiredlevelArray objectAtIndex:row] forState:UIControlStateNormal];
        pickerView.hidden=YES;
       
    }
    if (        (pickerView.tag == 103))
    {
        [alertTypeButton setTitle:[alertTypeArray objectAtIndex:row] forState:UIControlStateNormal];
        pickerView.hidden=YES;

    }
}


@end
