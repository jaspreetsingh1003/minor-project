//
//  DisclaimerView.m
//  BeAwake
//
//  Created by click labs136 on 9/24/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "DisclaimerView.h"
#import "selectOptionsView.h"

@implementation DisclaimerView
{
    NSString* disclaimerText;
     selectOptionsView* selectOptionsViewObject;
    UIButton *checkbutton;
}


-(void)disclaimerbar
{

    self.backgroundColor= [UIColor blackColor];
    
    UIView* labelViewOject= [[UIView alloc]initWithFrame:CGRectMake(0, 150, self.frame.size.width, 270)];
    [labelViewOject setBackgroundColor:[UIColor blackColor]];
    [self addSubview:labelViewOject];
    
    
    UILabel *iUnderstandlabel= [[UILabel alloc]initWithFrame:CGRectMake(25, 220, 100, 30)];
    [iUnderstandlabel setText:@"I Understand"];
    [iUnderstandlabel setTextColor:[UIColor whiteColor]];
    [iUnderstandlabel setBackgroundColor:[UIColor blackColor]];
    [labelViewOject addSubview:iUnderstandlabel];
    
    
    
    checkbutton=[[UIButton alloc]initWithFrame:CGRectMake(160, 225, 20, 20)];
    [checkbutton setBackgroundColor:[UIColor grayColor]];
    UIImage *uncheckButton= [UIImage imageNamed:@"check-empty-24-000000.png"];
    [checkbutton setImage:uncheckButton forState:UIControlStateNormal];
    [checkbutton addTarget:self action:@selector(checkButtonImageChangeAction) forControlEvents:UIControlEventTouchUpInside];
    [labelViewOject addSubview:checkbutton];
    
    
    UILabel *disclaimerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 200)];
    disclaimerLabel.numberOfLines=0;
    disclaimerLabel.backgroundColor= [UIColor grayColor];
    [disclaimerLabel setText:@"   Disclaimer\n\n  This app does not garuntee that it will \n  work!We are not liable for any \n  problems or accidents that occur when\n  using this app."];
    disclaimerLabel.textColor= [UIColor whiteColor];
    [labelViewOject addSubview:disclaimerLabel];
   
    
    UIButton* continuebutton= [[UIButton alloc]initWithFrame:CGRectMake(200, 220, 100, 30)];
    continuebutton.backgroundColor= [UIColor orangeColor];
    [continuebutton setTitle:@"Continue" forState:UIControlStateNormal];
    [continuebutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [continuebutton addTarget:self action:@selector(continuebuttonaction) forControlEvents:UIControlEventTouchUpInside];

    
    [labelViewOject addSubview:continuebutton];
    
    
    
}

-(void)checkButtonImageChangeAction
{
    
    [checkbutton setImage:[UIImage imageNamed:@"stock_form-checkbox.png"] forState:UIControlStateNormal];
}


-(void)continuebuttonaction
{
   selectOptionsViewObject = [[selectOptionsView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [selectOptionsViewObject selectOptionsViewMethod];
    
    [self addSubview:selectOptionsViewObject];
    
    
}


@end
