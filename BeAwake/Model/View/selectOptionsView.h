//
//  selectOptionsView.h
//  BeAwake
//
//  Created by click labs136 on 9/26/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface selectOptionsView : UIView<UIPickerViewDataSource,UIPickerViewDelegate>

-(void)selectOptionsViewMethod;
-(void)screenView;

@end
